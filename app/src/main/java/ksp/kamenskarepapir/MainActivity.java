package ksp.kamenskarepapir;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final KSPImageButton kImageButton = (KSPImageButton)findViewById(R.id.kamen);
        final KSPImageButton sImageButton = (KSPImageButton)findViewById(R.id.skare);
        final KSPImageButton pImageButton = (KSPImageButton)findViewById(R.id.papir);
        final KSPSignView pSignView = (KSPSignView)findViewById(R.id.playerSign);
        final KSPSignView eSignView = (KSPSignView)findViewById(R.id.enemySign);
        final TextView pScore = (TextView)findViewById(R.id.pScore);
        final TextView eScore = (TextView)findViewById(R.id.eScore);

        final ArrayList<KSPImageButton> choices = new ArrayList<>();
        choices.add(kImageButton);
        choices.add(sImageButton);
        choices.add(pImageButton);

        final ArrayList<KSPSignView> played = new ArrayList<>();
        played.add(pSignView);
        played.add(eSignView);

        final ArrayList<TextView> scores = new ArrayList<>();
        scores.add(pScore);
        scores.add(eScore);

        humanPlay(choices, played, scores);
    }

    public void humanPlay(final ArrayList<KSPImageButton> choices, final ArrayList<KSPSignView> played, final ArrayList<TextView> scores){

        for(final KSPImageButton kspImageButton:choices){
            kspImageButton.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View v) {
                    played.get(0).setImageView(kspImageButton);
                    compPlay(choices, played, scores);
                }
            });
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void compPlay(final ArrayList<KSPImageButton> choices, final ArrayList<KSPSignView> played, final ArrayList<TextView> scores){

        double random = Math.random()*3;
        int play =(int) random;
        played.get(1).setImageView(choices.get(play));

        score(choices, played, scores);
    }

    public void score(final ArrayList<KSPImageButton> choices, final ArrayList<KSPSignView> played, final ArrayList<TextView> scores){

        int pScore = Integer.parseInt(scores.get(0).getText().toString());
        int eScore = Integer.parseInt(scores.get(1).getText().toString());


        ImageView played1 = played.get(0).getImageView();
        ImageView played2 = played.get(1).getImageView();
        KSPImageButton kamen = choices.get(0);
        KSPImageButton skare = choices.get(1);
        KSPImageButton papir = choices.get(2);

        if(played1==kamen&&played2==skare){
            pScore++;
        } else if(played2==skare&&played2==kamen){
            eScore++;
        } else if(played1==kamen&&played2==papir){
            eScore++;
        } else if(played2==papir&&played1==kamen){
            pScore++;
        } else if(played1==skare&&played2==papir){
            pScore++;
        } else if(played2==skare&&played1==papir){
            eScore++;
        }

        String plScore = String.valueOf(pScore);
        String enScore = String.valueOf(eScore);

        scores.get(0).setText(plScore);
        scores.get(1).setText(enScore);

        humanPlay(choices, played, scores);
    }
}
