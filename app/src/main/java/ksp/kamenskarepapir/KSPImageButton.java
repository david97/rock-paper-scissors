package ksp.kamenskarepapir;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageButton;

/**
 * Created by dado on 20.06.16..
 * Image button with sign.
 */

public class KSPImageButton extends ImageButton {

    public KSPImageButton(Context context) {
        super(context);
    }

    public KSPImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KSPImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public Drawable getSign(){
        return this.getBackground();
    }
}
