package ksp.kamenskarepapir;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by dado on 20.06.16..
 * Sign that is played
 */

public class KSPSignView extends ImageView {

    private ImageView imageView;

    public KSPSignView(Context context) {
        super(context);
    }

    public KSPSignView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KSPSignView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void setImageView(ImageView imageView){
        this.imageView = imageView;
        if(imageView!=null) {
            this.setBackground(imageView.getBackground());
        } else {
            this.setBackground(null);
        }
    }

    public ImageView getImageView(){
        return imageView;
    }
}
